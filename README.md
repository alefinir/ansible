# A little Ansible playbook to deploy a static site with NGINX
---
This repo contains the code to build a static site using an Nginx server.

* inventory.cfg: global information about servers and users
* default: nginx config file to change root path of the service
* simple_deploy.yml: 
	* install nginx server
 	* replace default nginx config file with our file
 	* clear default webroot folder and clone the static site from a repo

---
## To run this script (ensure you have ssh sudo access for ubuntu user):

```sh
$ ansible-playbook -i inventory.cfg simple-deploy.yml
```

If you visit `http://ip_server` in your browser, you should be able to see the static site.